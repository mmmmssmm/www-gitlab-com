---
layout: handbook-page-toc
title: "TeamOps - Decision Velocity"
canonical_path: "/handbook/teamops/decision-velocity/"
description: TeamOps - Decision Velocity
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc}

- TOC
{:toc}

![GitLab TeamOps contribution illustration](/handbook/teamops/images/teamops-illustration_contribution_purple.png)
{: .shadow.medium.center}

This page is about one of the four Guiding Principles of TeamOps: Decision Velocity. To learn more about the other three Principles, return to the main TeamOps page for a [complete overview of TeamOps](/teamops/), or enroll in the free [TeamOps course](https://levelup.gitlab.com/learn/course/teamops). 
{: .alert .alert-info}

# Decision velocity

_Achieving faster, better results depends on decision-making velocity – a team’s ability to increase the quality and quantity of decisions made in a particular stretch of time through behavioral and logistical agreements._

Decisions are the fuel for high-performance teams, and represent a key success metric in the future of work. The more decisions are made, the more results can come from them. Conventional management philosophies often strive for consensus to avoid risk instead of developing a bias for action, which can result in delays, confusion, or unnecessary conflict.

In TeamOps, success is correlated with the group’s decision velocity, which is evidenced by the average duration of a collaboration process, and quality, value, or accuracy of the changes made from a decision. 

Action tenets of maximizing decision velocity, including real-world examples of each, are below.


## Collaboration is not consensus

TeamOps unlocks your organization's potential for making many decisions, by challenging the idea that consensus is productive. Organizations should strive to have smaller teams iterating quickly but transparently (allowing everyone to contribute), rather than a large team producing things slowly as they work toward consensus. 

Leaders and managers must moderate the desire to be involved in every decision. [Permissionless innovation](/handbook/values/#collaboration-is-not-consensus) increases a team's [bias for action](/handbook/values/#bias-for-action) and the number of decisions being made. If you choose the right [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) and empower them to work transparently, you should not expect them to wait for a brainstorming meeting or group sign-off.

Feedback should be documented transparently. The DRI must review all feedback, but they are not required to respond to everything. This can be challenging for teams and managers, when contributions and ideas don't receive a reply. However, the experience is far superior to decisions being made in private, with limited visibility and fewer opportunities for healthy discussion.

<details>
   <summary markdown="span">Example of collaboration is not consensus</summary>

   **Example 1:** [Implementing a replacement program for GitLab Contribute in FY23](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700)

   GitLab cancelled its FY23 Contribute event due to COVID risk posed to team members from a large, global event. Many decisions were necessary in order to implement a replacement initiative. This principle enabled the DRI (Directly Responsible Individual) to ingest a lot of thoughtful feedback in a [Manager Mention Merge Request for a FY23-Q3 Visiting Grant Program](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700). Although not every comment was replied to, everyone at the company was able to contribute feedback. Ultimately, the feedback was addressed and decisions were made in a 25-minute sync session, enabling GitLab team members to start planning their FY23-Q3 travel plans. 
</details>


## Give agency

Efficient execution [requires agency to be given](/handbook/values/#give-agency) by default. A critical component of workforce autonomy, agency empowers team members to independently and proactively make decisions without permission, review, or approval. In other words, to self-govern as a [manager of one](/handbook/leadership/#managers-of-one) It communicates that individuals are trusted to self-govern to accommodate their unique needs and design custom strategies to focus their time and attention on what they deem best.

It’s unrealistic to conflate a value of agency to assume that ***all*** organizational decisions will be made completely independently. Collaboration is still a critical value of TeamOps. But do all organizational decisions need collaboration? Strengthening agency in group dynamics can just start by removing rules or permissions for smaller operational components, like meeting attendance, personal task management systems, or working schedules. 

Agency is the antidote to micromanagement, which crushes execution, creativity, and retention. Instead, a shared reality that includes encouragement for each team member to design how and when they want to contribute fuels both individual and collective success. 


<details>
   <summary markdown="span">Example of give agency</summary>

   **Example 1:** [Normalizing that it's OK to look away in video calls](/company/culture/all-remote/meetings/#9-its-ok-to-look-away)

   Giving agency begins in the most typical of places. Video calls are a natural part of day-to-day work for many knowledge workers, yet cultural expectations about presenteeism and attentiveness may restrict agency. GitLab explicitly documents that [it's OK to look away](/company/culture/all-remote/meetings/#9-its-ok-to-look-away) during meetings and that no one should be embarrassed to occasionally ask for something to be repeated. By creating a culture where people are free to manage their own time and attention, they're able to direct energy on a minute-by-minute basis to execute. No one's path to execution looks the same. It may involve breaks to connect with friends, taking a walk outside, or watching a recording of a meeting during a more suitable time. 
</details>


## Bias for action

Ideation, collaboration, and execution are all accelerated when a team maintains a [bias for action](/handbook/values/#bias-for-action) as opposed to alternatives like alignment and consensus. This bias stems from the agency and ownership that each individual is empowered with, and can then use that autonomy to optimize their own proactivity, self-efficacy, and creativity. A conventional employee might have a mindset of, “Should I?” But a TeamOps participant can instead think, “I will.” 

When facing decisions that may involve imperfect information or failures, having a bias for action ensures a more rapid pace of execution. As a team, this may require a tolerance for mistakes and an appreciation for [two-way door decisions](/handbook/values/#make-two-way-door-decisions), which should be discussed as a shared reality and collaboration code.

<details>
   <summary markdown="span">Example of bias for action</summary>

   **Example 1:** [Coursera Remote Work course development](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1128)

   In September 2020, much of the knowledge-working world began to seek training on how to manage remote teams. As the COVID-19 pandemic forced many teams to work primarily from home, GitLab was well-positioned to provide proven practices for others to learn from and contribute back to. [Jessica R.](https://gitlab.com/jessicareeder)'s bias for action led to a question: "*What if we partner with a leader in online learning to teach the world a skill that it needs right now?*" This [GitLab epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1128) is a case study in execution, sparked by a bias for action. Every iteration is documented. The output is [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management), a free-to-take course on Coursera with 40,000 learners (and growing!). 
</details>


## Push decisions to the lowest possible level

As many decisions as possible should be made by the person doing the work (the DRI), not by their manager or their manager's manager. This ownership not only supports agency by empowering each person to directly and immediately make necessary changes to their work, but also increases efficiency by eliminating delays while waiting for approval, and frees senior leaders from the burden of making decisions that stunt their own productivity.

In the spirit of iteration, most times, it's better to execute quickly on a sub-optimal decision with full conviction (then return later to improve upon the decision based on post-decision feedback), rather than executing on a full decision with sub-optimal conviction. The DRI of each project knows the moving parts and impacts of a choice more than anyone else, and should be trusted with full accountability over it.

<details>
   <summary markdown="span">Example of push decisions to the lowest possible level</summary>

   **Example 1:** [Updating Developer Evangelism mentoring guidelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903)

   A Senior Developer Evangelist at GitLab recognized that many coaching and mentoring sessions are shared in private 1:1 conversations. In an effort to add context and transparency to the process — thereby enabling other developer evangelists to make more decisions on their own — he [documented and merged feedback examples](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903). The person doing the work is empowered to make the decision. In this example, the decision involved many micro decisions: to document or not; what context to add; where to document; what examples to share; how to share within the company. 
</details>


## Asynchronous workflows

Asynchronous work is about more than just freeing up calendar space. In TeamOps, asynchronous communication and workflows enable equal opportunities for team members to participate in conversations, receive updates, share insights, or deliver results no matter where or when they are working. In conventional organizations, information exchanges are tightly linked to meetings, which severely limits contributions to only those who are in attendance and are comfortable in a live, spontaneous discussion dynamic.

Instead of spending time scouring schedules and time zone differences to discuss something synchronously, shift the focus to creating clear documentation that will allow team members to contribute on their own time, and with more intentionality. This [gives agency](/handbook/values/#give-agency), reinforces a [bias for action](/handbook/values/#bias-for-action), and [bridges the knowledge gap](/company/culture/all-remote/asynchronous/#6-asynchronous-work-bridges-the-knowledge-gap), resulting in more total iterations. 

Establishing a thriving asynchronous culture also requires leaders to [celebrate incremental improvements](/company/culture/all-remote/asynchronous/#celebrate-incremental-improvements) and fight the urge to seek immediate gratification. To do this, they can encourage their team to strive for [iteration](/handbook/values/#iteration), transparency, and [progress, not perfection](/company/culture/all-remote/asynchronous/#aim-for-progress-not-perfection). 

<details>
   <summary markdown="span">Example of asynchronous workflows</summary>

   **Example 1:** [Adjusting expensable entries in GitLab's expense report policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471)

   In many organizations, altering the expense report policy would require — at minimum — one meeting. In an organization powered by TeamOps, the proposal [begins as documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471). Robert M., a senior engineer at GitLab, identified the need to reverse a policy change. He documented his reasoning in a merge request, and notified the Procurement team and other stakeholders. This allowed feedback to be gathered and the appropriate owners of the policy to consider the changes at a time that worked best for them. 

   Scaled across an organization, this meeting-free approach to making decisions enables more decisions to be made. This approach allows a more diverse array of perspectives to influence the decision, as there was no requirement to align 13 individuals to a single time slot on a given day for a synchronous meeting. 
</details>


## Boring solutions 

It is tempting to seek out the most cutting-edge, complex, or interesting solution to solve a problem. Instead, TeamOps encourages choosing ["boring" or simple solutions](/handbook/values/#boring-solutions). By taking every opportunity to reduce complexity in the organization, you're able to increase the speed and frequency of innovation.

One boring solution you may often see: researching what other successful organizations are doing and adopting their methods, rather than reinventing a process.

Embracing boring solutions and shipping the [minimum viable change (MVC)](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) also means [accepting mistakes](/handbook/values/#accept-mistakes) if that solution doesn't work, and moving on to the next [iteration](/handbook/values/#iteration). Because changes are small, mistakes are far less costly. This encourages more decision-making in a shorter span of time. 

<details>
   <summary markdown="span">Example of boring solutions</summary>

   **Example 1:** [Solving a GitLab attribution problem by improving git commit message](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130)

   Nick V., a director at GitLab, [proposed a boring solution](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130) to use existing functionality in new ways. By modifying a few lines in an automated message, he was able to solve an organization-wide problem with attribution. 

   In a TeamOps organization, boring solutions are celebrated *because of* their simplicity. There is always a possibility to add more polish or functionality, if it's needed in the future. The initial boring solution enables more decisions to be made, more quickly.
</details>


## Only healthy constraints 

Organizational growth does not have to result in stagnation. Leaders and managers must resist the tendency to put new processes and approvals are put in place because it's how other "mature" companies operate, slowing the pace of decision-making and innovation (e.g. [Slime Molds](https://komoroske.com/slime-mold/)).

TeamOps requires an awareness of the constraints that develop as a company grows, and an active approach toward removing them. TeamOps leaders must know the difference between [healthy (useful) constraints and unhealthy ones](/handbook/only-healthy-constraints/), and create a system for unblocking and reducing inefficiencies in the company. Ideally, a TeamOps organization can continue to operate with the agility of a startup while realizing the efficiencies of a scaling company.

Like giving agency to an individual, healthy constraints help an organization analyze the appropriate levels of approval, audit if blocks are surfacing unnecessarily, and intentionally design boundaries that will protect team growth instead of limiting it. 

<details>
   <summary markdown="span">Example of only healthy constraints</summary>

   **Example 1:** [GitLab's list of tactics to resist unhealthy constraints](/handbook/only-healthy-constraints/#resisting-unhealthy-constraints)

   In GitLab's S-1 filing, CEO Sid Sijbrandij documented some of the ways that GitLab plans to "remain a startup" and avoid the stagnation experienced by most early stage companies as they mature. These tactics are also [documented in the company handbook](/handbook/only-healthy-constraints/#resisting-unhealthy-constraints) as a living, iterating list of ideas and recommendations that anyone in the company can use to resist unhealthy constraints.
</details>


---

Return to the [TeamOps](/teamops/) homepage. 
